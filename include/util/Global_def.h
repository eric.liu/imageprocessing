#if !defined(AFX_GLOBAL_DEF_H)
#define AFX_GLOBAL_DEF_H

//#define LINUX_MODE 1
#define PI  3.1415926535897932384626433832795
#define MAX(a,b)  ((a) > (b) ? (a) : (b))
#define MIN(a,b)  ((a) < (b) ? (a) : (b))
#define max(a,b)  ((a) > (b) ? (a) : (b))
#define min(a,b)  ((a) < (b) ? (a) : (b))

typedef int             int_u32;
typedef int             int_s32;
typedef int             int_u16;
typedef int             int_s16;
typedef int             int_u8;
typedef int             int_s8;
typedef bool BOOL;
typedef unsigned char byte;
typedef unsigned char BYTE;



#endif
