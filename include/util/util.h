#ifndef UTIL_H
#define UTIL_H
namespace util {
	float GetSum(float* arr, int num);
	void DivideArray(float* arr, int num, float divide);
	float MultiplyArrayAndSum(float* arr1, float* arr2, int num);
	float GetMean(float* arr, int num);
	float GetStd(float* arr, int num);
	float GetMedian(float* arr, int num);
	float GetMin(float* arr, int num);
	float GetMax(float* arr, int num);
	float distance(float x, float y, float i, float j);
	float pow_distance(float x, float y, float i, float j);
	double gaussian(float x, double sigma);
	void ReSize(int iwidth, int iheight, int newwidth, int newheight, unsigned char* data, unsigned char* out);
	unsigned char fixpoint_cut_uc(int value, int bit);
	char fixpoint_cut_c(int value, int bit);
	void BoxYFilter(unsigned char* img, int w, int h, int fw, int fh, unsigned char* out);
	//void DownSample(unsigned char* in, unsigned char* out, int width, int height, BayerType type);
}
#endif