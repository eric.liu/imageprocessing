# ImageProcessing 

This is an image processing demo project , which can do image rotation

![alt tag](result.jpg)

## Windows Setup

### Requirements

 - Visual Studio 2015
 - [CMake](https://cmake.org/) 3.4 or higher (Visual Studio and [Ninja](https://ninja-build.org/) generators are supported)
 

### Building RawScaler (Windows)

```
> git clone https://gitlab.com/eric.liu/ImageProcessing.git
> cd $ImageProcessing_root/script
> script/build_win.cmd
> cd $ImageProcessing
> mkdir out
```

## Linux Setup

### Building ImageProcessing (Linux)

```
> git clone https://gitlab.com/eric.liu/ImageProcessing.git
> cd $ImageProcessing_root
> sh build.sh
> mkdir out
```

#### Build without cmake (Linux)

```
> git clone https://gitlab.com/eric.liu/ImageProcessing.git
> cd $ImageProcessing_root
> cp Makefile.cp Makefile
> mkdir out
> make all
```


### Windows Run 

```
> cd $ImageProcessing_root
> build\Debug\ImageRotation.exe (default image)
```


#### Linux Run

```
> cd $ImageProcessing_root
> ./ImageRotation 
```

the output file are stored in $ImageProcessing_root\out <br>

