
#include "util/Global_def.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <util/bitmap_image.hpp>

inline unsigned char Interpolation(unsigned char* img, int pad, float w, float h)
{
	float v[4] = { img[0],img[1],img[pad],img[pad + 1] };
	float wei[4] = { (1.0 - w)*(1.0 - h),w*(1.0 - h),(1.0 - w)*h,0 };
	wei[3] = 1.0 - wei[0] - wei[1] - wei[2];
	int r = v[0] * wei[0] + v[1] * wei[1] + v[2] * wei[2] + v[3] * wei[3];
	if (r>255) r=255; else if(r<0) r=0;
	return r;
}
void rot_coor_transform(int ix, int iy,float sinma,float cosma, int center_x, int center_y, float &ox, float &oy) {
	ox = (cosma * ix - sinma * iy) + center_x;
	oy = (sinma * ix + cosma * iy) + center_y;
}
void check_coor_min_max(int ox,int oy,int &maxx,int &minx,int &maxy,int &miny)
{
	if (ox > maxx) {
		maxx = ox;
	}
	if (ox < minx) {
		minx = ox;
	}
	if (oy > maxy) {
		maxy = oy;
	}
	if (oy < miny) {
		miny = oy;
	}
}
void estimation_output_size(int width, int height,int &new_opx,int &new_opy, int &new_width, int &new_height, float sinma, float cosma, int center_x, int center_y)
{
	int minx = 99999;
	int miny = 99999;
	int maxx = 0;
	int maxy = 0;
	float ox, oy;

	rot_coor_transform(0, 0, sinma, cosma, center_x, center_y, ox, oy);
	check_coor_min_max(ox, oy, maxx, minx, maxy, miny);

	rot_coor_transform(width - 1, 0, sinma, cosma, center_x, center_y, ox, oy);
	check_coor_min_max(ox, oy, maxx, minx, maxy, miny);

	rot_coor_transform(0, height - 1, sinma, cosma, center_x, center_y, ox, oy);
	check_coor_min_max(ox, oy, maxx, minx, maxy, miny);

	rot_coor_transform(width - 1, height - 1, sinma, cosma, center_x, center_y, ox, oy);
	check_coor_min_max(ox, oy, maxx, minx, maxy, miny);

	int dx = center_x;
	int dy = center_y;
	rot_coor_transform(dx, dy, sinma, cosma, center_x, center_y, ox, oy);

	new_width = maxx - minx;
	new_height = maxy - miny;

	new_opx = new_width / 2;
	new_opy = new_height / 2;
}
BYTE *rotate_image(BYTE* img, int width, int height, int rot, int &new_width, int &new_height,int center_x,int center_y) {

	float rotation_theta = (float)rot*PI / 180.0;
	double sinma = sin(rotation_theta); //forward transform
	double cosma = cos(rotation_theta);
	int newop_x, newop_y;
	estimation_output_size(width, height, newop_x, newop_y, new_width, new_height, sinma, cosma, center_x, center_y);
	sinma = sin(-rotation_theta); //backward transform
	cosma = cos(-rotation_theta);
	unsigned char* ret_img = new unsigned char[new_width*new_height];


	float xin_f, yin_f;
	for (int x = 0; x < new_width; x++) {
		for (int y = 0; y < new_height; y++) {
			int out_idx = y*new_width + x;
			
			int xout = x - newop_x;
			int yout = y - newop_y;
			rot_coor_transform(xout, yout, sinma, cosma, center_x, center_y, xin_f, yin_f); //backward transform

			int xin = (int)floor(xin_f); 
			int yin = (int)floor(yin_f);

			int in_idx = yin*width + xin;
			if (xin >= 0 && xin < width-1 && yin >= 0 && yin < height-1) {
				
				ret_img[out_idx] = Interpolation(&img[in_idx], width, xin_f - (float)xin, yin_f - (float)yin); 
				//ret_img[out_idx] = img[in_idx];
			}
			else {

				ret_img[out_idx] = 0;
			}
		}
	}
	//*new_width = estimate width;
	//*new_height = estimate height;
	//BYTE *ret_img = malloc((*new_width) * (*new_height));
	//fill data into ret_img
	return ret_img;
}


BYTE* bitmap_image_to_gray(bitmap_image src)
{
	int w = src.get_width();
	int h = src.get_height();
	//#pragma omp parallel for
	unsigned char* img = new unsigned char[w*h];
	for (int i = 0; i < h; i++) {
		int img_index = i*w;
		for (int j = 0; j < w; j++)
		{
			rgb_t t = src.get_pixel(j, i);
			unsigned char gray = (t.red + t.blue + t.green)/3;
			img[img_index + j] = gray;
		}
	}
	return img;
}
int main() {

#if defined(OutputDir) && defined(ImagePath)
	char dir[128];
	sprintf(dir, "%s", OutputDir);
	bitmap_image image = bitmap_image(ImagePath);
	
	int in_width = image.get_width();
	int in_height = image.get_height();
	unsigned char* img = bitmap_image_to_gray(image);
	int out_height = in_height;
	int out_width = in_width;
	unsigned char* process_img = rotate_image(img, in_width, in_height, 45, out_width, out_height, in_width / 2, in_height / 2);

	bitmap_image out_image(out_width, out_height);
	int w = out_width;
	for (int i = 0; i < out_height; i++) {
		for (int j = 0; j < out_width; j++) {
			int val = process_img[i*out_width + j];
			out_image.set_pixel(j, i, val, val, val);
		}
	}
	sprintf(dir, "%s", OutputDir);
	out_image.save_image(strcat(dir, "result.bmp"));




#else

#endif

}
