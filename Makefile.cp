#------------------------------------------------------------------------------

SOURCE=tools/ImageRotation/ImageRotation.cpp include/util/util.h src/util/util.cpp 
MYPROGRAM=ImageRotation
MY_UTIL=include/util/
MYINCLUDES=include/

CC=g++

#------------------------------------------------------------------------------



all: $(MYPROGRAM)



$(MYPROGRAM): $(SOURCE)

	$(CC) -I$(MY_UTIL) -I$(MYINCLUDES) $(SOURCE) -o$(MYPROGRAM) -DOutputDir=\"out//\" -DImagePath=\"data//kodim19.bmp\" -DLINUX_MODE

clean:

	rm -f $(MYPROGRAM)